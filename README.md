# GitLab Meetup CW13, 2020-03-24

> **Note**
>
> The most recent workshop can be found [here](https://handbook.gitlab.com/handbook/marketing/developer-relations/developer-advocacy/projects/#workshops) and includes more learning practice for GitLab CI/CD, based on this first version. 

The presentation by [Michael Friedrich](https://gitlab.com/dnsmichi): [Make your life as a developer easier with CI/CD and the Web IDE](https://docs.google.com/presentation/d/e/2PACX-1vS3l6cy_6HDdoaYviuJWhFZTJRMjeQyBZ8Tr_lUuAB_DBb6mowPUiBfUwFpgpzScc_GwYPKO-oQMcpO/pub?start=false&loop=false&delayms=3000) includes exercises for self-practice. Start learning GitLab CI/CD and tweet @dnsmichi with your experiences and screenshots!

The CI config solution is provided in the [.gitlab-ci.solution.yml](.gitlab-ci.solution.yml) file in case you need assistance from the exercises.

Recording: https://www.youtube.com/watch?v=l5705U8s_nQ 

If you have any questions for our community team, you can contact @johncoghlan 

Hope to see you at our next GitLab remote meetup!

